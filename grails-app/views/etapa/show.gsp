
<%@ page import="h4f.puzzle.Etapa" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'etapa.label', default: 'Etapa')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-etapa" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="etapa.titulo.label" default="Titulo" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: etapaInstance, field: "titulo")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="etapa.actividades.label" default="Actividades" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${etapaInstance.actividades}" var="a">
						<li><g:link controller="actividad" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>


<%@ page import="h4f.puzzle.Etapa" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'etapa.label', default: 'Etapa')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-etapa" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="titulo" title="${message(code: 'etapa.titulo.label', default: 'Titulo')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${etapaInstanceList}" status="i" var="etapaInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${etapaInstance.id}">${fieldValue(bean: etapaInstance, field: "titulo")}</g:link></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${etapaInstanceTotal}" />
	</div>
</section>

</body>

</html>

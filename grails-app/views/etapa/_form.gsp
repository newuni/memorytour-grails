<%@ page import="h4f.puzzle.Etapa" %>



			<div class="control-group fieldcontain ${hasErrors(bean: etapaInstance, field: 'titulo', 'error')} required">
				<label for="titulo" class="control-label"><g:message code="etapa.titulo.label" default="Titulo" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="titulo" required="" value="${etapaInstance?.titulo}"/>
					<span class="help-inline">${hasErrors(bean: etapaInstance, field: 'titulo', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: etapaInstance, field: 'actividades', 'error')} ">
				<label for="actividades" class="control-label"><g:message code="etapa.actividades.label" default="Actividades" /></label>
				<div class="controls">
					<g:select name="actividades" from="${h4f.puzzle.Actividad.list()}" multiple="multiple" optionKey="id" size="5" value="${etapaInstance?.actividades*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: etapaInstance, field: 'actividades', 'error')}</span>
				</div>
			</div>


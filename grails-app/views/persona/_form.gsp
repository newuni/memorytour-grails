<%@ page import="h4f.puzzle.Persona" %>



			<div class="control-group fieldcontain ${hasErrors(bean: personaInstance, field: 'nombre', 'error')} required">
				<label for="nombre" class="control-label"><g:message code="persona.nombre.label" default="Nombre" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="nombre" required="" value="${personaInstance?.nombre}"/>
					<span class="help-inline">${hasErrors(bean: personaInstance, field: 'nombre', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: personaInstance, field: 'etapas', 'error')} ">
				<label for="etapas" class="control-label"><g:message code="persona.etapas.label" default="Etapas" /></label>
				<div class="controls">
					<g:select name="etapas" from="${h4f.puzzle.Etapa.list()}" multiple="multiple" optionKey="id" size="5" value="${personaInstance?.etapas*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: personaInstance, field: 'etapas', 'error')}</span>
				</div>
			</div>


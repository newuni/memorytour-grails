
<%@ page import="h4f.puzzle.Persona" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'persona.label', default: 'Persona')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-persona" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="persona.nombre.label" default="Nombre" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: personaInstance, field: "nombre")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="persona.etapas.label" default="Etapas" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${personaInstance.etapas}" var="e">
						<li><g:link controller="etapa" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>

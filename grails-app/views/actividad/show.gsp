
<%@ page import="h4f.puzzle.Actividad" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'actividad.label', default: 'Actividad')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-actividad" class="first">

	<table class="table">
		<tbody>
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.titulo.label" default="Titulo" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "titulo")}</td>
			</tr>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.tipologia.label" default="Tipologia" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "tipologia")}</td>
			</tr>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.introduccion.label" default="Introduccion" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "introduccion")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.pregunta.label" default="Pregunta" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "pregunta")}</td>
			</tr>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.respuesta1.label" default="Respuesta 1" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "respuesta1")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.respuesta2.label" default="Respuesta 2" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "respuesta2")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.respuesta3.label" default="Respuesta 3" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "respuesta3")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.puzzleImageUrl.label" default="Puzzle Image Url" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "puzzleImageUrl")}</td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.videoUrl.label" default="Video Url" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "videoUrl")}</td>
			</tr>
			
			<tr class="prop">
				<td valign="top" class="name"><g:message code="actividad.musicaUrl.label" default="Musica Url" /></td>
				<td valign="top" class="value">${fieldValue(bean: actividadInstance, field: "musicaUrl")}</td>
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>

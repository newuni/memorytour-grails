
<%@ page import="h4f.puzzle.Actividad" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'actividad.label', default: 'Actividad')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-actividad" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="titulo" title="${message(code: 'actividad.titulo.label', default: 'Título')}" />
			
				<g:sortableColumn property="tipologia" title="${message(code: 'actividad.tipologia.label', default: 'Tipología')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${actividadInstanceList}" status="i" var="actividadInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${actividadInstance.id}">${fieldValue(bean: actividadInstance, field: "titulo")}</g:link></td>
			
				<td>${fieldValue(bean: actividadInstance, field: "tipologia")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${actividadInstanceTotal}" />
	</div>
</section>

</body>

</html>

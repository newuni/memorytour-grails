<%@ page import="h4f.puzzle.Actividad" %>

			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'titulo', 'error')} ">
				<label for="titulo" class="control-label"><g:message code="actividad.titulo.label" default="Titulo" /></label>
				<div class="controls">
					<g:textField name="titulo" value="${actividadInstance?.titulo}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'titulo', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'tipologia', 'error')} ">
				<label for="tipologia" class="control-label"><g:message code="actividad.tipologia.label" default="Tipologia" /></label>
				<div class="controls">
					<g:textField name="tipologia" value="${actividadInstance?.tipologia}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'tipologia', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'introduccion', 'error')} ">
				<label for="introduccion" class="control-label"><g:message code="actividad.introduccion.label" default="Introduccion" /></label>
				<div class="controls">
					<g:textArea name="introduccion" value="${actividadInstance?.introduccion}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'introduccion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'pregunta', 'error')} ">
				<label for="pregunta" class="control-label"><g:message code="actividad.pregunta.label" default="Pregunta" /></label>
				<div class="controls">
					<g:textArea name="pregunta" value="${actividadInstance?.pregunta}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'pregunta', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'respuesta1', 'error')} ">
				<label for="respuesta1" class="control-label"><g:message code="actividad.respuesta1.label" default="Respuesta1" /></label>
				<div class="controls">
					<g:textField name="respuesta1" value="${actividadInstance?.respuesta1}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'respuesta1', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'respuesta2', 'error')} ">
				<label for="respuesta2" class="control-label"><g:message code="actividad.respuesta2.label" default="Respuesta2" /></label>
				<div class="controls">
					<g:textField name="respuesta2" value="${actividadInstance?.respuesta2}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'respuesta2', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'respuesta3', 'error')} ">
				<label for="respuesta3" class="control-label"><g:message code="actividad.respuesta3.label" default="Respuesta3" /></label>
				<div class="controls">
					<g:textField name="respuesta3" value="${actividadInstance?.respuesta3}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'respuesta3', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'respCorrecta', 'error')} required">
				<label for="respCorrecta" class="control-label"><g:message code="actividad.respCorrecta.label" default="Resp Correcta" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="number" name="respCorrecta" required="" value="${actividadInstance.respCorrecta}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'respCorrecta', 'error')}</span>
				</div>
			</div>
			
			
			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'puzzleImageUrl', 'error')} ">
				<label for="puzzleImageUrl" class="control-label"><g:message code="actividad.puzzleImageUrl.label" default="Puzzle Image Url" /></label>
				<div class="controls">
					<g:textField name="puzzleImageUrl" value="${actividadInstance?.puzzleImageUrl}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'puzzleImageUrl', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'musicaUrl', 'error')} ">
				<label for="musicaUrl" class="control-label"><g:message code="actividad.musicaUrl.label" default="Musica Url" /></label>
				<div class="controls">
					<g:textField name="musicaUrl" value="${actividadInstance?.musicaUrl}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'musicaUrl', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: actividadInstance, field: 'videoUrl', 'error')} ">
				<label for="videoUrl" class="control-label"><g:message code="actividad.videoUrl.label" default="Video Url" /></label>
				<div class="controls">
					<g:textField name="videoUrl" value="${actividadInstance?.videoUrl}"/>
					<span class="help-inline">${hasErrors(bean: actividadInstance, field: 'videoUrl', 'error')}</span>
				</div>
			</div>


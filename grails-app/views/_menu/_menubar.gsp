<g:if test="${session.layout == null || session.layout == 'grid'}">
	<g:set var="menutype" value="nav nav-tabs" />
</g:if>
<g:elseif test="${session.layout == 'fluid'}">
	<g:set var="menutype" value="nav nav-tabs" />
</g:elseif>

<!-- position of menu: LTR (left-to-right, European) or RTL (right-to-left, Oriental) -->
<g:if test="${session.menuposition == 'right' && session.layout == 'grid'}">
	<g:set var="menuposition" value="pull-right" />
</g:if>
<g:elseif test="${session.menuposition == 'right' && session.layout == 'fluid'}">
	<g:set var="menuposition" value="tabbable tabs-right" /> <!-- pull-right -->
</g:elseif>
<g:elseif test="${session.layout == 'fluid'}">
	<g:set var="menuposition" value="tabbable tabs-left" /> <!-- pull-left -->
</g:elseif>
<g:else>
	<g:set var="menuposition" value="" />
</g:else>


<div class="${menuposition}">
	<ul class="${menutype}" data-role="listview" data-split-icon="gear" data-filter="true">
		<li class="controller${params.controller == "persona" ? " active" : ""}"><g:link controller="persona" action="index">Personas</g:link></li>
		<li class="controller${params.controller == "etapa" ? " active" : ""}"><g:link controller="etapa" action="index">Etapas vitales</g:link></li>
		<li class="controller${params.controller == "actividad" ? " active" : ""}"><g:link controller="actividad" action="index">Actividades</g:link></li>
		
	</ul>
</div>

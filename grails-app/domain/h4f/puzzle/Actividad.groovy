package h4f.puzzle

/**
 * Actividad
 * A domain class describes the data object and it's mapping to the database
 */
class Actividad {
	
	public static final String TEXTO = "texto"
	public static final String PUZZLE = "puzzle"
	public static final String MUSICA = "musica"
	public static final String VIDEO = "video"
	
	public static final def TIPOLOGIAS = [ TEXTO, PUZZLE, MUSICA, VIDEO ]
	
	String titulo
			
	String tipologia
	
	// tipo texto
	String introduccion
	
	//tipo puzzle
	String puzzleImageUrl
	
	//tipo video
	String videoUrl
	
	//tipo musica
	String musicaUrl
				
	String pregunta
	String respuesta1
	String respuesta2
	String respuesta3
	
	Integer respCorrecta
	
//	Date	dateCreated
//	Date	lastUpdated

	static belongsTo = [Etapa]
	
    static mapping = {
//		autoTimestamp true
    }
    
	static constraints = {
    }
	
	
	String toString() {
		return titulo;
	}	
	
}

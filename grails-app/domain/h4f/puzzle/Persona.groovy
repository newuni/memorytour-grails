package h4f.puzzle

/**
 * Persona
 * A domain class describes the data object and it's mapping to the database
 */
class Persona {
	
	String nombre

//	Date	dateCreated
//	Date	lastUpdated
	
	static hasMany		= [etapas: Etapa]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
	
    static mapping = {
//		autoTimestamp true
    }
    
	static constraints = {
		nombre blank:false
    }
	
}

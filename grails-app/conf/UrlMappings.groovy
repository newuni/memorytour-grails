class UrlMappings {

	static mappings = {
		
		/* 
		 * Pages without controller 
		 */
//		"/"				(view:"/index")
		"/about"		(view:"/siteinfo/about")
		"/blog"			(view:"/siteinfo/blog")
		"/systeminfo"	(view:"/siteinfo/systeminfo")
		"/contact"		(view:"/siteinfo/contact")
		"/terms"		(view:"/siteinfo/terms")
		"/imprint"		(view:"/siteinfo/imprint")
		
        "/"	{
			controller	= 'home'
			action		= { 'index' }
            view		= { 'index' }
        }
		"/$controller/$action?/$id?"{
		}
		
		// API URL, una ruta por metodo
		"/api/personas"(controller: "api", action: [GET: "personas"])
		"/api/persona"(controller: "api", action: [GET: "persona"])
		"/api/etapa"(controller: "api", action: [GET: "etapa"])
		"/api/actividad"(controller: "api", action: [GET: "actividad"])
		"/api/actividadByEtapa"(controller: "api", action: [GET: "actividadByEtapa"])
		
		
		/* 
		 * System Pages without controller 
		 */
		"403"	(view:'/_errors/403')
		"404"	(view:'/_errors/404')
		"500"	(view:'/_errors/error')
		"503"	(view:'/_errors/503')
	}
}

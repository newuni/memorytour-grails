package h4f.puzzle

import org.springframework.dao.DataIntegrityViolationException

/**
 * ActividadController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ActividadController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [actividadInstanceList: Actividad.list(params), actividadInstanceTotal: Actividad.count()]
    }

    def create() {
        [actividadInstance: new Actividad(params)]
    }

    def save() {
        def actividadInstance = new Actividad(params)
        if (!actividadInstance.save(flush: true)) {
            render(view: "create", model: [actividadInstance: actividadInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'actividad.label', default: 'Actividad'), actividadInstance.id])
        redirect(action: "show", id: actividadInstance.id)
    }

    def show() {
        def actividadInstance = Actividad.get(params.id)
        if (!actividadInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'actividad.label', default: 'Actividad'), params.id])
            redirect(action: "list")
            return
        }

        [actividadInstance: actividadInstance]
    }

    def edit() {
        def actividadInstance = Actividad.get(params.id)
        if (!actividadInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'actividad.label', default: 'Actividad'), params.id])
            redirect(action: "list")
            return
        }

        [actividadInstance: actividadInstance]
    }

    def update() {
        def actividadInstance = Actividad.get(params.id)
        if (!actividadInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'actividad.label', default: 'Actividad'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (actividadInstance.version > version) {
                actividadInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'actividad.label', default: 'Actividad')] as Object[],
                          "Another user has updated this Actividad while you were editing")
                render(view: "edit", model: [actividadInstance: actividadInstance])
                return
            }
        }

        actividadInstance.properties = params

        if (!actividadInstance.save(flush: true)) {
            render(view: "edit", model: [actividadInstance: actividadInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'actividad.label', default: 'Actividad'), actividadInstance.id])
        redirect(action: "show", id: actividadInstance.id)
    }

    def delete() {
        def actividadInstance = Actividad.get(params.id)
        if (!actividadInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'actividad.label', default: 'Actividad'), params.id])
            redirect(action: "list")
            return
        }

        try {
            actividadInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'actividad.label', default: 'Actividad'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'actividad.label', default: 'Actividad'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}

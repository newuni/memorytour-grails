package h4f.puzzle

import grails.converters.JSON

/**
 * API
 */
class ApiController {

	
	def personas(){
		
		def personas = Persona.getAll()
		render personas as JSON
	}
	
	def persona(){
		def persona = Persona.load(params.int('id'))
		render persona as JSON
	}
	
	def etapa(){
		def etapa = Etapa.load(params.int('id'))
		render etapa as JSON
	}
	
	def actividad() {
		def actividad = Actividad.load(params.int('id'))
		render actividad as JSON
	}
	
	def actividadesByEtapa() {
		def etapa = Etapa.load(params.int('id'))
		render etapa.actividades as JSON
	}
	
	
}
